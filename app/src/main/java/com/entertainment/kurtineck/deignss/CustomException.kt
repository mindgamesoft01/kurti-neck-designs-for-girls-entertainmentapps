package com.entertainment.kurtineck.deignss

class CustomException(message: String) : Exception(message)
